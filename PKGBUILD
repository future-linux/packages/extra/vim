# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=(vim-runtime vim gvim)
pkgbase=vim
pkgver=9.1.0776
pkgrel=1
pkgdesc="Vi Improved, a highly configurable, improved version of the vi text editor"
arch=('x86_64')
url="https://www.vim.org/"
license=('custom:vim')
depends=('glibc' 'gawk' 'acl' 'zlib' 'gpm')
makedepends=('gawk' 'glibc' 'gpm' 'gtk3' 'libcanberra' 'libgcrypt' 'libxt'
 	'lua' 'perl' 'python' 'ruby' 'tcl' 'zlib')
source=(https://github.com/vim/vim/archive/v${pkgver}/${pkgbase}-${pkgver}.tar.gz
	vimrc
	futurelinux.vim
	vimdoc.hook)
sha256sums=(124a9b41d94c60bc6238594ba060ccab86d18ce5229ff2ec35fcb4ad9ca9cfcb
	fc18ce4e9fcf267105f6c2b3e7d149d7763bb13ca330f73b4c05582c432318fa
	a07fda83bb43a3aaa4ee9c7790ba88af23e396d3e89bbb8cac5ece0b1b8e1e37
	8e9656934d9d7793063230d15a689e10455e6db9b9fe73afa0f294792795d8ae)

prepare() {

	(
		cd ${pkgbase}-${pkgver}/src

		# define the place for the global (g)vimrc file (set to /etc/vimrc)
		sed -E 's|^.*(#define SYS_.*VIMRC_FILE.*").*$|\1|g' -i feature.h
		sed -E 's|^.*(#define VIMRC_FILE.*").*$|\1|g' -i feature.h

		autoconf
	)

	cp -a ${pkgbase}-${pkgver} g${pkgbase}-${pkgver}
}

build() {

	(
		cd ${pkgbase}-${pkgver}

		${CONFIGURE}                          \
		    --localstatedir=/var/lib/vim      \
		    --with-features=huge              \
		    --with-compiledby='Future Linux'  \
		    --enable-gpm                      \
		    --enable-acl                      \
		    --with-x=no                       \
		    --disable-gui                     \
		    --enable-multibyte                \
		    --enable-cscope                   \
		    --enable-netbeans                 \
		    --enable-perlinterp=dynamic       \
		    --enable-python3interp=dynamic    \
		    --enable-rubyinterp=dynamic       \
		    --enable-luainterp=dynamic        \
		    --enable-tclinterp=dynamic        \
		    --disable-canberra

		make
	)

	(
		cd g${pkgbase}-${pkgver}

		${CONFIGURE}                         \
		    --localstatedir=/var/lib/vim     \
		    --with-features=huge             \
		    --with-compiledby='Future Linux' \
		    --enable-gpm                     \
		    --enable-acl                     \
		    --with-x=yes                     \
		    --enable-gui=gtk3                \
		    --enable-multibyte               \
		    --enable-cscope                  \
		    --enable-netbeans                \
		    --enable-perlinterp=dynamic      \
		    --enable-python3interp=dynamic   \
		    --enable-rubyinterp=dynamic      \
		    --enable-luainterp=dynamic       \
		    --enable-tclinterp=dynamic       \
		    --enable-canberra

		make
	)
}

package_vim-runtime() {
	pkgdesc+=" (shared runtime)"
	backup=(etc/vimrc)

	cd ${pkgbase}-${pkgver}

	make -j1 VIMRCLOC=/etc DESTDIR=${pkgdir} install

	# man and bin files belong to 'vim'
	rm -r ${pkgdir}/usr/share/man/ ${pkgdir}/usr/bin/

	# Don't forget logtalk.dict
	install -vDm644 runtime/ftplugin/logtalk.dict ${pkgdir}/usr/share/vim/vim91/ftplugin/logtalk.dict

	# rc files
	install -vDm644 ${srcdir}/vimrc ${pkgdir}/etc/vimrc
	install -vDm644 ${srcdir}/futurelinux.vim ${pkgdir}/usr/share/vim/vimfiles/futurelinux.vim

	# no desktop files and icons
	rm -r ${pkgdir}/usr/share/{applications,icons}

}

package_vim() {
	depends=("vim-runtime=${pkgver}-${pkgrel}" 'gpm' 'acl' 'glibc' 'libgcrypt' 'zlib')

	cd ${pkgbase}-${pkgver}

	make -j1 VIMRCLOC=/etc DESTDIR=${pkgdir} install

	# provided by (n)vi in core
	rm ${pkgdir}/usr/bin/{ex,view}

	# delete some manpages
	find ${pkgdir}/usr/share/man -type d -name 'man1' 2>/dev/null | \
		while read _mandir; do
		cd ${_mandir}
		rm -f ex.1 view.1 # provided by (n)vi
		rm -f evim.1    # this does not make sense if we have no GUI
	done

	# Runtime provided by runtime package
	rm -r ${pkgdir}/usr/share/vim

	# remove gvim.desktop as not included
	rm ${pkgdir}/usr/share/applications/gvim.desktop

	# pacman hook for documentation helptags
	install -Dm 644 ${srcdir}/vimdoc.hook ${pkgdir}/usr/share/libalpm/hooks/vimdoc.hook

}

package_gvim() {
	pkgdesc+=" (with advanced features, such as a GUI)"
	depends=("vim-runtime=${pkgver}-${pkgrel}" 'gpm' 'libxt' 'gtk3' 'glibc' 'libgcrypt'
		'zlib' 'libcanberra')

	cd g${pkgbase}-${pkgver}

	make -j1 VIMRCLOC=/etc DESTDIR=${pkgdir} install

	# provided by (n)vi in core
	rm ${pkgdir}/usr/bin/{ex,view}

	# delete some manpages
	find ${pkgdir}/usr/share/man -type d -name 'man1' 2>/dev/null | \
		while read _mandir; do
		cd ${_mandir}
		rm -f ex.1 view.1 # provided by (n)vi
	done

	# need to remove since this is provided by vim-runtime
	rm -r ${pkgdir}/usr/share/vim

	# pacman hook for documentation helptags
	install -vDm644 ${srcdir}/vimdoc.hook ${pkgdir}/usr/share/libalpm/hooks/vimdoc.hook
}
